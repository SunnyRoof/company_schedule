from datetime import datetime

weekday_fields = {
    0: 'monday',
    1: 'tuesday',
    2: 'wednesday',
    3: 'thursday',
    4: 'friday',
    5: 'saturday',
    6: 'sunday',
}


def now_in_interval(interval, now):
    return interval[0] < now.time() < interval[1]


def status_in_day(slots, now):
    status = False
    for slot in slots:
        status = now_in_interval(slot, now) | status
    return status


def works_now(company, now=datetime.now()):
    weekday = now.weekday()
    field_name = weekday_fields[weekday]
    field_value = getattr(company, field_name)
    return status_in_day(field_value, now)
