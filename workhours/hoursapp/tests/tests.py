from hoursapp.utils import works_now
from hoursapp.models import Company
from datetime import datetime, time
from django.test import TestCase
from django.contrib.auth.models import User

class TimeTestCase(TestCase):
    """
    test methods MUST be named as test_{whatever}
    or else django won't find them, sadly
    """
    def setUp(self):
        User.objects.create_user(username='tester', email='test@test.test', password='tester')
        user = User.objects.get(username='tester')
        monday = [[time(0, 0), time(17, 20)], [time(17, 40), time(20, 0)], [time(0, 0), time(0, 0)]]
        tuesday = [[time(0, 0), time(15, 0)], [time(0, 0), time(0, 0)], [time(0, 0), time(0, 0)]]
        wednesday = [[time(0, 0), time(20, 0)], [time(0, 0), time(0, 0)], [time(0, 0), time(0, 0)]]
        thursday = [[time(0, 0), time(0, 0)], [time(0, 0), time(0, 0)], [time(0, 0), time(0, 0)]]
        friday = [[time(0, 0), time(0, 0)], [time(0, 0), time(0, 0)], [time(0, 0), time(0, 0)]]
        saturday = [[time(0, 0), time(0, 0)], [time(0, 0), time(0, 0)], [time(0, 0), time(0, 0)]]
        sunday = [[time(0, 0), time(0, 0)], [time(0, 0), time(0, 0)], [time(0, 0), time(0, 0)]]

        self.test_company = Company(name='test',
                                    owner=user,
                                    monday=monday,
                                    tuesday=tuesday,
                                    wednesday=wednesday,
                                    thursday=thursday,
                                    friday=friday,
                                    saturday=saturday,
                                    sunday=sunday)

    def tearDown(self):
        pass

    def test_is_open(self):

        monday = datetime.strptime('Feb 19 2018  17:23', '%b %d %Y %H:%M')
        tuesday = datetime.strptime('Feb 20 2018  14:33', '%b %d %Y %H:%M')
        wednesday = datetime.strptime('Feb 21 2018  20:33', '%b %d %Y %H:%M')
        sunday = datetime.strptime('Feb 18 2018  12:33', '%b %d %Y %H:%M')
        print('testing!')
        self.assertEqual(works_now(self.test_company, monday), False)
        self.assertEqual(works_now(self.test_company, tuesday), True)
        self.assertEqual(works_now(self.test_company, wednesday), False)
        self.assertEqual(works_now(self.test_company, sunday), False)
