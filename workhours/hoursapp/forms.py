from django import forms
from .models import Company


class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = ('name', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday')


class ArrayForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['monday'].delimiter = ';'
        self.fields['tuesday'].delimiter = ';'
        self.fields['wednesday'].delimiter = ';'
        self.fields['thursday'].delimiter = ';'
        self.fields['friday'].delimiter = ';'
        self.fields['saturday'].delimiter = ';'
        self.fields['sunday'].delimiter = ';'

    class Meta:
        model = Company
        fields = '__all__'

