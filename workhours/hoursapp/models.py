from django.db import models
from datetime import time
from django.contrib.postgres.fields import ArrayField


class Company(models.Model):
    owner = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    monday = ArrayField(ArrayField(models.TimeField(default=time(0, 0)), size=2), size=3)
    tuesday = ArrayField(ArrayField(models.TimeField(default=time(0, 0)), size=2), size=3)
    wednesday = ArrayField(ArrayField(models.TimeField(default=time(0, 0)), size=2), size=3)
    thursday = ArrayField(ArrayField(models.TimeField(default=time(0, 0)), size=2), size=3)
    friday = ArrayField(ArrayField(models.TimeField(default=time(0, 0)), size=2), size=3)
    saturday = ArrayField(ArrayField(models.TimeField(default=time(0, 0)), size=2), size=3)
    sunday = ArrayField(ArrayField(models.TimeField(default=time(0, 0)), size=2), size=3)

    def __str__(self):
        return self.name
