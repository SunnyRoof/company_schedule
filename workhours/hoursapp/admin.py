from django.contrib import admin
from .forms import ArrayForm
from .models import Company


@admin.register(Company)
class ArrayAdmin(admin.ModelAdmin):
    form = ArrayForm