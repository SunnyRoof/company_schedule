from .serializers import CompanySerializer
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import detail_route, list_route
from rest_framework import viewsets
from .utils import works_now
from .models import Company


class CompanyViewSet(viewsets.ModelViewSet):
    """
    A simple company schedule app
    """
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    @detail_route(methods=['get'])
    def working(self, request, pk=None):
        """
        Determine if company is working right now
        """
        company_obj = self.get_object()
        if works_now(company_obj):
            resp = {'status': 'open'}
        else:
            resp = {'status': 'closed'}
        return JsonResponse(resp, safe=False)