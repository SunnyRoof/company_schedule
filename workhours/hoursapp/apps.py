from django.apps import AppConfig


class HoursappConfig(AppConfig):
    name = 'hoursapp'
