from rest_framework import serializers
from .models import Company


class CompanySerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Company
        fields = ('name', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday',)

    def create(self, validated_data):
        """
        test create
        """
        return Company.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        For now we update only monday, see if it works
        """
        instance.name = validated_data.get('name', instance.name)
        instance.monday = validated_data.get('monday', instance.monday)
        instance.save()
        return instance
